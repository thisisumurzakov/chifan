from django.db import models

from account.models import User
from main.models import cTradeSubject


class cOrder(models.Model):
    RecID = models.AutoField(primary_key=True)
    RecDTCreate = models.DateTimeField(auto_now_add=True, null=True)
    ###
    RecSum = models.FloatField(default=-1, null=True)
    RecPaymentMant = models.IntegerField(default=-1, null=True)  # храним мантису для учета и выявления соответствия
    RecUserFIO = models.CharField(max_length=1000, default='', null=True)
    RecWeChatID = models.CharField(max_length=1000, default='', null=True)
    RecTelNum = models.CharField(max_length=1000, default='', null=True)
    RecUserDocID = models.CharField(max_length=1000, default='', null=True)
    RecLogisticInfo = models.CharField(max_length=10000, default='', null=True)
    RecStatus = models.IntegerField(default=-1,
                                    null=True)  # -1 - заведен, но не закончен, 0 - не оплачен, 1 - оплачен, 2 - отправлен, 3 доставлен
    RecPriceTotal = models.DecimalField(max_digits=10, decimal_places=2, default=0.0, null=True)
    RecTxID = models.CharField(max_length=200, default='', null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='orders')
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.0, null=True)

    # RecGroupName
    def strStatus(self):
        strStat = ''
        if self.RecStatus == -1:
            strStat = 'Не подтвержден'
        elif self.RecStatus == 0:
            strStat = 'Не оплачен'
        elif self.RecStatus == 1:
            strStat = 'Оплачен'
        elif self.RecStatus == 2:
            strStat = 'В доставке'
        elif self.RecStatus == 3:
            strStat = 'Выдан'
        else:
            strStat = 'Неизвестен'
        return strStat

    def as_dict(self):
        orders = []
        try:
            SpecOrders = cSpecOrder.objects.filter(RecOrderID=self.RecID).order_by('-RecID')
            orders = [order.as_dict() for order in SpecOrders]
        except Exception as ex:
            print(ex)

        strStat = ''
        if self.RecStatus == -1:
            strStat = 'Не подтвержден'
        elif self.RecStatus == 0:
            strStat = 'Не оплачен'
        elif self.RecStatus == 1:
            strStat = 'Оплачен'
        elif self.RecStatus == 2:
            strStat = 'В доставке'
        elif self.RecStatus == 3:
            strStat = 'Выдан'
        else:
            strStat = 'Неизвестен'

        return {
            'RecID': self.RecID
            , 'RecDTCreate': self.RecDTCreate
            , 'RecSum': self.RecSum
            , 'RecPaymentMant': self.RecPaymentMant
            , 'RecUserDocID': self.RecUserDocID
            , 'RecUserFIO': self.RecUserFIO
            , 'RecWeChatID': self.RecWeChatID
            , 'RecTelNum': self.RecTelNum
            , 'RecLogisticInfo': self.RecLogisticInfo
            , 'Orders': orders
            , 'RecStatus': self.RecStatus
            , 'strStatus': strStat
            , 'RecTxID': self.RecTxID
            , 'RecPriceTotal': self.RecPriceTotal
        }


class cSpecOrder(models.Model):
    RecID = models.AutoField(primary_key=True)
    RecDTCreate = models.DateTimeField(auto_now_add=True, null=True)
    ###
    RecOrderID = models.ForeignKey(cOrder, on_delete=models.CASCADE, null=True)
    RecTradeSubjectId = models.ForeignKey(cTradeSubject, on_delete=models.CASCADE, null=True)
    RecAmount = models.FloatField(default=0, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def as_dict(self):
        good = {}
        try:
            tradeSubject = cTradeSubject.objects.get(RecID=self.RecTradeSubjectId)
            good = tradeSubject.as_dict()
        except Exception as ex:
            print(ex)
        return {
            'RecID': self.RecID
            , 'RecGood': good
            , 'RecAmount': self.RecAmount
        }

    def get_cost(self):
        return self.price * self.RecAmount
