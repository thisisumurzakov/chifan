from django import forms
from .models import cOrder

class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = cOrder
        fields = ['RecLogisticInfo',]