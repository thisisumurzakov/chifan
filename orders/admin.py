from django.contrib import admin
from .models import cOrder, cSpecOrder

class OrderItemInline(admin.TabularInline):
    model = cSpecOrder
    raw_id_fields = ['RecTradeSubjectId']

@admin.register(cOrder)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['RecID', 'RecUserFIO', 'RecWeChatID', 'RecTelNum',
    'RecLogisticInfo', 'RecUserDocID', 'RecStatus',
    'RecDTCreate']
    list_filter = ['RecStatus', 'RecDTCreate']
    inlines = [OrderItemInline]