from time import time

from orders.models import cOrder

from django.shortcuts import render
from .models import cSpecOrder
from .forms import OrderCreateForm
from cart.cart import Cart

def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            order.user = request.user
            order.total_price = cart.get_total_price()
            order.save()

            # mantisa
            mantisa = '000000'
            total = '0.' + mantisa
            orderNum = str(order.RecID)

            mantisa = int(str(round(time())) + (orderNum))
            total = '0.00' + str(mantisa)
            order.RecSum = round(order.total_price) + 1
            order.RecPaymentMant = mantisa
            order.RecPriceTotal = round(order.total_price) + float(total)
            order.RecStatus = 0
            order.save()
            for item in cart:
                cSpecOrder.objects.create(RecOrderID=order,
                                          RecTradeSubjectId=item['product'],
                                          price=item['price'],
                                          RecAmount=item['quantity'])
            # Очищаем корзину.
            cart.clear()
            return render(request, 'orders/created.html', {'order': order})
    else:
        form = OrderCreateForm()
    return render(request, 'orders/create.html', {'cart': cart, 'form': form})

def ConfirmOrder(request):
    try:
        print('ConfirmOrder')
        rGoodId = request.POST['goodId']
        print(rGoodId)
        rAmount = request.POST['amount']
        print(rAmount)
        rOrderNum = request.POST['orderNum']
        print(rOrderNum)
        rFIO = request.POST['FIO']
        print(rFIO)
        rUserDocID = request.POST['UserDocID']
        print(rUserDocID)
        rLogisticInfo = request.POST['LogisticInfo']
        print(rLogisticInfo)
        rWeChatID = request.POST['WeChatID']
        print(rWeChatID)
        rTelNum = request.POST['telNum']
        print(rTelNum)

        orderNum = '000000'
        mantisa = '000000'
        total = '0.' + mantisa

        order = cOrder()
        order.save()
        orderNum = str(order.RecID)

        ind = 0
        totalSum = 0.0

        print(rGoodId)
        good = cTradeSubject.objects.get(RecID=int(rGoodId))

        spec = cSpecOrder()
        spec.RecOrderID = int(orderNum)
        spec.RecTradeSubjectId = good.RecID
        spec.RecAmount = float(rAmount)
        totalSum = totalSum + (spec.RecAmount * good.RecPrice)
        spec.save()

        mantisa = int(str(round(time())) + (orderNum))
        total = '0.00' + str(mantisa)
        order.RecSum = round(totalSum) + 1
        order.RecPaymentMant = mantisa
        order.RecPriceTotal = round(totalSum) + float(total)
        total = str(order.RecPriceTotal)
        order.save()

        order.RecUserFIO = rFIO
        order.RecWeChatID = rWeChatID
        order.RecTelNum = rTelNum
        order.RecUserDocID = rUserDocID
        order.RecLogisticInfo = rLogisticInfo
        order.RecStatus = 0

        order.save()

        return JsonResponse({'status': 'OK', 'details': 'OK', 'OrderNum': order.RecID, 'mantisa': order.RecPaymentMant,
                             'total': str(order.RecPriceTotal), 'order': order.as_dict()})
    except Exception as ex:
        print(ex)
        print('finishSubscribe with error')
        return JsonResponse({'status': 'ERROR', 'details': ex})
    print('finishSubscribe as hs')
    return JsonResponse({'status': 'UNKNOWN', 'details': 'UNKNOWN'})
