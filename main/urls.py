from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('v1/', views.IndexView1.as_view(), name='v1'),
    path('v2/', views.IndexView2.as_view()),
    path('<int:pk>/', views.CatalogView.as_view()),
    path('search/', views.SearchView.as_view(), name='search'),
]
