from django.contrib import admin

from .models import cGoodsGroup, cDocsGood, cTradeSubject

admin.site.register(cGoodsGroup),
admin.site.register(cDocsGood),
admin.site.register(cTradeSubject)