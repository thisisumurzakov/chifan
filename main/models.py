import datetime

from django.db import models


class cGoodsGroup(models.Model):
    RecID = models.AutoField(primary_key=True)
    RecDTCreate = models.DateTimeField(auto_now_add=True, null=True)
    RecName = models.CharField(max_length=1000, default='', null=True)
    RecParentGroupId = models.ForeignKey('self', on_delete=models.CASCADE, related_name='subcategories', null=True, blank=True)
    RecPict = models.FileField(upload_to='images/', null=True, blank=True)
    slug = models.SlugField(unique=True)

    # RecGroupName
    @property
    def RecGroupName(self):
        parentName = ''
        try:
            groupParent = cGoodsGroup.objects.get(RecID=self.RecParentGroupId)
            parentName = groupParent.RecName
        except Exception as ex:
            print(ex)
            parentName = ''
        return parentName

    def as_dict(self):
        parentName = ''
        if self.RecParentGroupId > -1:
            try:
                groupParent = cGoodsGroup.objects.get(RecID=self.RecParentGroupId)
                parentName = groupParent.RecName
            except Exception as ex:
                print(ex)
                parentName = ''
        try:
            pictUrl = self.RecPict.url
        except:
            pictUrl = ''

        return {
            'RecID': self.RecID
            , 'RecName': self.RecName
            , 'RecParentGroupId': self.RecParentGroupId
            , 'RecParentGroupName': parentName
            , 'RecPict': pictUrl
        }

    def __str__(self):
        return self.RecName


class cTradeSubject(models.Model):
    RecID = models.AutoField(primary_key=True)
    RecDTCreate = models.DateTimeField(auto_now_add=True, null=True)
    RecGroupId = models.ForeignKey(cGoodsGroup, on_delete=models.CASCADE, related_name='products', null=True)
    RecName = models.CharField(max_length=1000, default='', null=True)
    RecMinAmount = models.CharField(max_length=1000, default='', null=True)
    RecAddrRecipient = models.CharField(max_length=100, default='', null=True)
    RecPict = models.FileField(upload_to='images/', null=True)
    RecPrice = models.DecimalField(max_digits=10, decimal_places=2, default=0.0, null=True)
    RecWeight = models.FloatField(default=0.0, null=True)  # в кг

    # RecGroupName
    def RecGroupName(self):
        parentName = ''
        try:
            group = cGoodsGroup.objects.get(RecID=self.RecGroupId)
            parentName = group.RecName
        except Exception as ex:
            print(ex)
            parentName = ''
        return parentName

    def as_dict(self):
        parentName = ''
        if self.RecGroupId > -1:
            try:
                groupParent = cGoodsGroup.objects.get(RecID=self.RecGroupId)
                parentName = groupParent.RecName
            except Exception as ex:
                print(ex)
                parentName = ''
        pictUrl = ''
        try:
            pictUrl = self.RecPict.url
        except:
            pictUrl = ''

        return {
            'RecID': self.RecID
            , 'RecDTCreate': self.RecDTCreate
            , 'RecName': self.RecName
            , 'RecGroupId': self.RecGroupId
            , 'RecGroupName': parentName
            , 'RecMinAmount': self.RecMinAmount
            , 'RecAddrRecipient': self.RecAddrRecipient
            , 'RecPict': pictUrl
            , 'RecPrice': self.RecPrice
            , 'RecWeight': self.RecWeight

        }

    class Meta:
        ordering = ('-RecDTCreate',)


class cDocsGood(models.Model):
    RecID = models.AutoField(primary_key=True)
    RecDTCreate = models.DateTimeField(auto_now_add=True, null=True)
    ###
    RecTradeSubjectId = models.ForeignKey(cTradeSubject, on_delete=models.CASCADE, related_name='pics', null=True)
    RecPict = models.FileField(upload_to='images/', null=True)