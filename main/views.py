from django.db.models import Prefetch
from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.base import View

from .models import cGoodsGroup, cTradeSubject
from cart.forms import CartAddProductForm

class IndexView2(View):
    def get(self, request):
        categories = cGoodsGroup.objects.filter(RecParentGroupId=None)
        cart_product_form = CartAddProductForm()
        products = cTradeSubject.objects.all().select_related('RecGroupId__RecParentGroupId')
        return render(request, "index2.html", {"categories": categories,
                                               'products': products,
                                               'cart_product_form': cart_product_form})

class IndexView1(View):
    def get(self, request):
        categories = cGoodsGroup.objects.filter(RecParentGroupId=None)
        cart_product_form = CartAddProductForm()
        data = {}
        for c in categories:
            products = cTradeSubject.objects.filter(RecGroupId__RecParentGroupId=c).order_by('-RecDTCreate')
            data[c.RecName] = products
        return render(request, "index.html", {"categories": categories,
                                              'data': data,
                                              'cart_product_form': cart_product_form})


class CatalogView(View):
    def get(self, request, pk):
        cart_product_form = CartAddProductForm()
        category = cGoodsGroup.objects.prefetch_related(Prefetch('subcategories__products', queryset=cTradeSubject.objects.order_by('-RecDTCreate'))).get(RecID=pk)
        return render(request, 'catalog.html', {"category": category,
                                                'cart_product_form': cart_product_form})

class SearchView(View):
    def get(self, request):
        cart_product_form = CartAddProductForm()
        products = cTradeSubject.objects.filter(RecName__icontains=self.request.GET.get('q'))
        return render(request, 'main/search.html', {'products': products,
                                                    'cart_product_form': cart_product_form})