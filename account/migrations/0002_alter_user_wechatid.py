# Generated by Django 4.0.3 on 2022-03-24 05:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='wechatid',
            field=models.CharField(max_length=20, unique=True, verbose_name='WeChat ID'),
        ),
    ]
