from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _

class UserManager(BaseUserManager):
    def _create_user(self, wechatid, password, **extra_fields):
        if not wechatid:
            raise ValueError("The given phone number must be set")

        user = self.model(wechatid=wechatid, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self,wechatid, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(wechatid, password, **extra_fields)

    def create_superuser(self, wechatid, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(wechatid, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_('first name'), max_length=150, null=True)
    last_name = models.CharField(_('last name'), max_length=150, null=True)
    birth_date = models.DateField(_('date of birth'), null=True)
    wechatid = models.CharField('WeChat ID', max_length=20, unique=True)
    phone_number = models.CharField(max_length=16, unique=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    username = None
    REQUIRED_FIELDS = []
    USERNAME_FIELD = "wechatid"
    objects = UserManager()
    is_validated = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.id} {self.first_name} {self.last_name}"